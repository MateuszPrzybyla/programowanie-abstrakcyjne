# README #

Rozwiazania zadan z cwiczen Programowania Abstrakcyjnego (semestr zimowy 2015/2016).

Autor: **Mateusz Przybyla**

### Konfiguracja ###

* Potrzebne narzedzia
- JDK 8
- Maven 3

### Odpalanie testow ###
Po zainstalowaniu JDK i Mavena, w celu odpalenia testow prosze przejsc do katalogu z projektem i wywolac polecenie
```
mvn clean test
```

Aby odpalic testy zwiazane tylko z jednymi cwiczeniami prosze wywolac z tego samego katalogu polecenie
```
mvn clean test -Dtest=cwXX.*
```
gdzie w miejsce XX nalezy wstawic dwucyfrowy numer cwiczen (np. **mvn clean test -Dtest=cw01.* **)


### Contact ###
- Ewentualny kontakt prosze kierowac pod adres przybyla.mat@gmail.com