package cw06;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ControllerWithDependencyInjectionTest {
    @Mock
    private IObjectsConstructor objectsConstructor;
    @Mock
    private IConstructionRecipeCreator constructionRecipeCreator;
    @Mock
    private ILogger logger;
    @Mock
    private IProductionLineMover productionLineMover;
    @Mock
    private ConstructionRecipe constructionRecipe;

    private String nameOfObject = "UJ's first car";
    private ControllerTemplateMethod controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new ControllerWithDependencyInjection(objectsConstructor, constructionRecipeCreator, logger,
            productionLineMover);
    }

    @Test
    public void testConstructionOfSingleObject() {
        when(constructionRecipe.getNameOfObject()).thenReturn(nameOfObject);
        when(constructionRecipeCreator.getConstructionRecipe()).thenReturn(constructionRecipe);
        when(constructionRecipeCreator.getNumberOfElementsToProduce()).thenReturn(1);
        when(productionLineMover.moveProductionLine(eq(MovingDirection.FORWARD))).thenReturn(true);
        when(objectsConstructor.constructObjectFromRecipe(eq(constructionRecipe))).thenReturn(true);

        controller.execute();

        verify(constructionRecipe).getNameOfObject();
        verify(constructionRecipeCreator).getConstructionRecipe();
        verify(objectsConstructor).constructObjectFromRecipe(eq(constructionRecipe));
        verify(logger).log(eq(LoggingType.INFO), anyString());
        verify(productionLineMover, times(2)).moveProductionLine(eq(MovingDirection.FORWARD));
    }

    @Test
    public void testConstructionFailureMovingProductionLineFailed() {
        when(constructionRecipe.getNameOfObject()).thenReturn(nameOfObject);
        when(constructionRecipeCreator.getConstructionRecipe()).thenReturn(constructionRecipe);
        when(constructionRecipeCreator.getNumberOfElementsToProduce()).thenReturn(1);
        when(productionLineMover.moveProductionLine(eq(MovingDirection.FORWARD))).thenThrow(new RuntimeException(":("));

        controller.execute();

        verify(constructionRecipe).getNameOfObject();
        verify(constructionRecipeCreator).getConstructionRecipe();
        verify(objectsConstructor, never()).constructObjectFromRecipe(eq(constructionRecipe));
        verify(logger, never()).log(eq(LoggingType.INFO), anyString());
        verify(productionLineMover).moveProductionLine(eq(MovingDirection.FORWARD));
    }

    @Test
    public void testConstructionFailureObjectConstructionFailed() {
        when(constructionRecipe.getNameOfObject()).thenReturn(nameOfObject);
        when(constructionRecipeCreator.getConstructionRecipe()).thenReturn(constructionRecipe);
        when(constructionRecipeCreator.getNumberOfElementsToProduce()).thenReturn(1);
        when(productionLineMover.moveProductionLine(eq(MovingDirection.FORWARD))).thenReturn(true);
        when(objectsConstructor.constructObjectFromRecipe(eq(constructionRecipe))).thenReturn(false);
        when(productionLineMover.moveProductionLine(eq(MovingDirection.TO_SCAN))).thenReturn(true);

        controller.execute();

        verify(constructionRecipe).getNameOfObject();
        verify(constructionRecipeCreator).getConstructionRecipe();
        verify(objectsConstructor).constructObjectFromRecipe(eq(constructionRecipe));
        verify(logger).log(eq(LoggingType.WARNING), anyString());
        verify(productionLineMover).moveProductionLine(eq(MovingDirection.FORWARD));
        verify(productionLineMover).moveProductionLine(eq(MovingDirection.TO_SCAN));
    }

    @Test
    public void testConstructionFailureObjectConstructionFailedThenMovingToScanAlsoFailed() {
        when(constructionRecipe.getNameOfObject()).thenReturn(nameOfObject);
        when(constructionRecipeCreator.getConstructionRecipe()).thenReturn(constructionRecipe);
        when(constructionRecipeCreator.getNumberOfElementsToProduce()).thenReturn(1);
        when(productionLineMover.moveProductionLine(eq(MovingDirection.FORWARD))).thenReturn(true);
        when(objectsConstructor.constructObjectFromRecipe(eq(constructionRecipe))).thenReturn(false);
        when(productionLineMover.moveProductionLine(eq(MovingDirection.TO_SCAN))).thenThrow(new RuntimeException(":("));

        controller.execute();

        verify(constructionRecipe).getNameOfObject();
        verify(constructionRecipeCreator).getConstructionRecipe();
        verify(objectsConstructor).constructObjectFromRecipe(eq(constructionRecipe));
        verify(logger).log(eq(LoggingType.WARNING), anyString());
        verify(logger).log(eq(LoggingType.ERROR), anyString());
        verify(productionLineMover).moveProductionLine(eq(MovingDirection.FORWARD));
        verify(productionLineMover).moveProductionLine(eq(MovingDirection.TO_SCAN));
    }
}