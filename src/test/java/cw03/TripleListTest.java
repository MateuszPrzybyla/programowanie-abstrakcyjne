package cw03;

import java.util.Arrays;
import java.util.stream.IntStream;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class TripleListTest {
    @Test
    public void testEmptyListCreation() {
        TripleList<Integer> tripleList = new TripleList<>();

        assertEquals(0, tripleList.size());
        assertNull(tripleList.getPreviousElement());
        assertNull(tripleList.getMiddleElement());
        assertNull(tripleList.getNextElement());
    }

    @Test
    public void testAddingSingleElement() {
        TripleList<Integer> tripleList = new TripleList<>();
        final Integer value = 4;
        tripleList.add(value);

        assertEquals(1, tripleList.size());
        assertEquals(value, tripleList.getValue());
        assertNull(tripleList.getPreviousElement());
        assertNull(tripleList.getMiddleElement());
        assertNull(tripleList.getNextElement());
    }

    @Test
    public void testAddingTwoElements() {
        TripleList<Integer> tripleList = new TripleList<>();
        final Integer value1 = 4;
        final Integer value2 = -9;
        tripleList.add(value1);
        tripleList.add(value2);

        assertEquals(2, tripleList.size());

        assertEquals(value1, tripleList.getValue());
        assertEquals(value2, tripleList.getMiddleElement().getValue());
        assertEquals(tripleList.getValue(), tripleList.getMiddleElement().getMiddleElement().getValue());

        assertNull(tripleList.getPreviousElement());
        assertNotNull(tripleList.getMiddleElement());
        assertNull(tripleList.getNextElement());

        assertNull(tripleList.getMiddleElement().getPreviousElement());
        assertNull(tripleList.getMiddleElement().getNextElement());
    }

    @Test
    public void testAddingThreeElements() {
        TripleList<Integer> tripleList = new TripleList<>();
        final Integer value1 = 4;
        final Integer value2 = -9;
        final Integer value3 = 47;
        tripleList.add(value1);
        tripleList.add(value2);
        tripleList.add(value3);

        assertEquals(3, tripleList.size());

        assertEquals(value1, tripleList.getValue());
        assertEquals(value2, tripleList.getMiddleElement().getValue());
        assertEquals(value3, tripleList.getNextElement().getValue());

        assertNull(tripleList.getPreviousElement());
        assertNotNull(tripleList.getMiddleElement());
        assertNotNull(tripleList.getNextElement());

        assertNull(tripleList.getMiddleElement().getPreviousElement());
        assertNotNull(tripleList.getMiddleElement().getMiddleElement());
        assertNull(tripleList.getMiddleElement().getNextElement());

        assertNotNull(tripleList.getNextElement().getPreviousElement());
        assertNull(tripleList.getNextElement().getMiddleElement());
        assertNull(tripleList.getNextElement().getNextElement());

        assertEquals(value1, tripleList.getValue());
        assertEquals(value2, tripleList.getMiddleElement().getValue());
        assertEquals(value3, tripleList.getNextElement().getValue());
    }

    @Test
    public void testAddingFiveElements() {
        TripleList<Integer> tripleList = new TripleList<>();
        IntStream.rangeClosed(1, 5).forEach(tripleList::add);

        assertEquals(5, tripleList.size());

        assertEquals(Integer.valueOf(1), tripleList.getValue());
        assertEquals(Integer.valueOf(2), tripleList.getMiddleElement().getValue());
        assertEquals(Integer.valueOf(3), tripleList.getNextElement().getValue());
        assertEquals(Integer.valueOf(4), tripleList.getNextElement().getMiddleElement().getValue());
        assertEquals(Integer.valueOf(5), tripleList.getNextElement().getNextElement().getValue());
    }

    @Test
    public void testListsEnumerator() {
        double[] values = {1.1, 3.14, 6.13, 9.9999, 99.001};
        TripleList<Double> tripleList = new TripleList<>();
        Arrays.stream(values).forEach(tripleList::add);
        int i = 0;
        for (double d : tripleList) {
            assertEquals(values[i++], d, 0.00001);
        }
    }

    @Test
    public void testIfNoCycle() {
        final int NUMBER_OF_ELEMENTS = 100;
        TripleList<Integer> tripleList = new TripleList<>();
        IntStream.range(0, NUMBER_OF_ELEMENTS).forEach(tripleList::add);

        TripleList<Integer> tripleListEverySingleNode = tripleList;
        TripleList<Integer> tripleListEveryTwoNodes = tripleList.getNextElement();
        for (int i = 0; i < NUMBER_OF_ELEMENTS * NUMBER_OF_ELEMENTS; i++) {
            assertNotEquals(tripleListEverySingleNode, tripleListEveryTwoNodes);
            tripleListEverySingleNode = jumpToNextElement(tripleListEverySingleNode);
            if (tripleListEveryTwoNodes.getNextElement() == null) {
                break;
            } else {
                tripleListEveryTwoNodes = tripleListEveryTwoNodes.getNextElement();
            }
        }
    }

    private TripleList<Integer> jumpToNextElement(TripleList<Integer> element) {
        if (isNotLastElement(element)) {
            if (isMiddleElement(element)) {
                if (element.getMiddleElement().getNextElement() != null) {
                    return element.getMiddleElement().getNextElement();
                }
            } else {
                if (element.getNextElement() != null) {
                    return element.getNextElement();
                }
            }
        }
        return null;
    }

    private boolean isNotLastElement(TripleList<Integer> element) {
        return element.getMiddleElement() != null;
    }

    private boolean isMiddleElement(TripleList<Integer> element) {
        return element.getNextElement() == null && element.getPreviousElement() == null
            && element.getMiddleElement() != null;
    }

}