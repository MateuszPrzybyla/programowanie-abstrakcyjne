package cw01;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestSuite {
    @Test
    public void testExporter() {
        String textToBeExported = "Ala ma kota";
        Exporter exporter = new TextExporter(textToBeExported);
        Data exportedData = exporter.getExportData();
        String exportedText = ((TextData)exportedData).getText();
        assertEquals(textToBeExported, exportedText);
        exportedData = exporter.getExportData();
        exportedText = ((TextData)exportedData).getText();
        textToBeExported = "";
        assertEquals(textToBeExported, exportedText);
    }

    @Test
    public void testImporter() {
        String textToBeImported = "Ala zgubila dolara";
        Data dataToSendToImporter = new TextData(textToBeImported);
        Importer importer = new TextImporter();
        importer.importData(dataToSendToImporter);
        String dataSavedInImporter = ((TextImporter)importer).getImportedText();
        assertEquals(textToBeImported, dataSavedInImporter);
    }

    @Test
    public void testFactory() {
        String textToFactory = "Ali kot zjadl dolara";
        DistributedModuleFactory factory = new DistributedModuleFactory(textToFactory);
        Data dataFromFactory = factory.createData();
        String textFromModule = ((TextData) dataFromFactory).getText();
        assertEquals(textToFactory, textFromModule);
        Exporter exporter = factory.createExporter();
        textFromModule = ((TextExporter) exporter).getExportData().getText();
        assertEquals(textToFactory, textFromModule);
        Importer importer = factory.createImporter();
        assertTrue(importer instanceof TextImporter);
    }
}