package cw02;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.apache.commons.collections4.IteratorUtils;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TreeTest {
    @Test
    public void iterableValidation() {
        List<Class<?>> implementedInterfaces = Arrays.asList(Tree.class.getInterfaces());
        assertTrue(implementedInterfaces.contains(Iterable.class));

        Tree<Integer> subtree = new Tree<>(5, EnumeratorOrder.BFS, Arrays.asList(1, 2));
        Tree<Integer> tree = new Tree<>(7, EnumeratorOrder.BFS, Arrays.asList(subtree, 10, 15));
        assertEquals(Integer.valueOf(10), getStream(tree).filter(i -> i % 2 == 0).findFirst().get());
        assertEquals(Arrays.asList(7, 5, 10, 15, 1, 2), IteratorUtils.toList(tree.iterator()));
        tree.setOrder(EnumeratorOrder.DFS);
        assertEquals(Arrays.asList(7, 5, 1, 2, 10, 15), IteratorUtils.toList(tree.iterator()));
    }

    @Test
    public void iterateWithNoChildren() {
        Tree<Integer> tree = new Tree<>(7, EnumeratorOrder.DFS);
        assertEquals(Integer.valueOf(7), tree.iterator().next());
        tree.setOrder(EnumeratorOrder.BFS);
        assertEquals(Integer.valueOf(7), tree.iterator().next());
        Tree<Integer> subtree = new Tree<>(5, EnumeratorOrder.BFS);
        tree.add(subtree);
        assertEquals(Integer.valueOf(5), getLast(tree.iterator()));
        tree.setOrder(EnumeratorOrder.DFS);
        assertEquals(Integer.valueOf(5), getLast(tree.iterator()));
    }

    @Test
    public void orderPropertyValidation() {
        Tree<Integer> subtree = new Tree<>(5, EnumeratorOrder.DFS, Arrays.asList(1, 2));
        Tree<Integer> tree = new Tree<>(7, EnumeratorOrder.BFS);
        tree.add(subtree);
        assertEquals(EnumeratorOrder.BFS, subtree.getOrder());
        for (Tree<Integer> child : subtree.getChildren()) {
            assertEquals(EnumeratorOrder.BFS, child.getOrder());
        }
        subtree.add(3);
        assertEquals(EnumeratorOrder.BFS,
            subtree.getChildren().stream().filter(i -> i.getValue() == 3).findFirst().get().getOrder());
        tree.setOrder(EnumeratorOrder.DFS);
        subtree.add(4);
        assertEquals(EnumeratorOrder.DFS,
            subtree.getChildren().stream().filter(i -> i.getValue() == 4).findFirst().get().getOrder());
    }

    private Integer getLast(Iterator<Integer> iterator) {
        Integer lastValue = null;
        while (iterator.hasNext()) {
            lastValue = iterator.next();
        }
        return lastValue;
    }

    private Stream<Integer> getStream(Tree<Integer> tree) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(tree.iterator(), Spliterator.NONNULL), false);
    }
}