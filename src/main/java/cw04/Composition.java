package cw04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Composition<T> implements Executable<T>, Iterable<Executable<T>> {
    private List<Executable<T>> functions;

    @SafeVarargs
    public Composition(Executable<T>... functions) {
        this.functions = new ArrayList<>(Arrays.asList(functions));
    }

    public Composition(Collection<Executable<T>> functions) {
        this.functions = new ArrayList<>(functions);
    }

    public void add(Executable<T> function) {
        this.functions.add(function);
    }

    @Override
    public T execute(T arg) {
        T result = arg;
        for (Executable<T> function : functions) {
            result = function.execute(result);
        }
        return result;
    }

    @Override
    public Iterator<Executable<T>> iterator() {
        return functions.iterator();
    }
}
