package cw04;

public interface Executable<T> {
    T execute(T arg);
}
