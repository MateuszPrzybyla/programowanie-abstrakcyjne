package cw06;

public class ConstructionRecipe {
    private String nameOfObject;

    public ConstructionRecipe(String nameOfObject) {
        this.nameOfObject = nameOfObject;
    }

    public String getNameOfObject() {
        return nameOfObject;
    }
}
