package cw06;

public interface IObjectsConstructor {
    boolean constructObjectFromRecipe(ConstructionRecipe recipe);
}
