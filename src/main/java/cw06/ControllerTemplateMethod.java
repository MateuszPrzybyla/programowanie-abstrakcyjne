package cw06;

public abstract class ControllerTemplateMethod {
    public void execute() {
        ConstructionRecipe constructionRecipe = this.getConstructionRecipe();
        String nameOfObject = constructionRecipe.getNameOfObject();
        for (int i = 0; i < this.getNumberOfElementsToProduce(); i++) {
            try {
                if (this.moveProductionLine(MovingDirection.FORWARD)) {
                    if (this.constructObject(constructionRecipe)) {
                        this.log(LoggingType.INFO, "Object [" + nameOfObject + "] has been created!");
                        this.moveProductionLine(MovingDirection.FORWARD);
                    } else {
                        this.log(LoggingType.WARNING, "Failed to construct object [" + nameOfObject + "]!");
                        try {
                            this.moveProductionLine(MovingDirection.TO_SCAN);
                        } catch (RuntimeException e) {
                            this.log(LoggingType.ERROR, e.getMessage());
                        }
                    }
                }
            } catch (RuntimeException e) {
                this.log(LoggingType.ERROR, e.getMessage());
            }
        }
    }

    protected abstract ConstructionRecipe getConstructionRecipe();

    protected abstract boolean constructObject(ConstructionRecipe constructionRecipe);

    protected abstract int getNumberOfElementsToProduce();

    protected abstract void log(LoggingType info, String msg);

    protected abstract boolean moveProductionLine(MovingDirection movingDirection);

}
