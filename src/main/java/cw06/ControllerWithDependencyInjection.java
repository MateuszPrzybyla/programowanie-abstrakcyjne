package cw06;

public class ControllerWithDependencyInjection extends ControllerTemplateMethod {
    private ILogger logger;
    private IProductionLineMover productionLineMover;
    private IConstructionRecipeCreator constructionRecipeCreator;
    private IObjectsConstructor objectsConstructor;

    public ControllerWithDependencyInjection(IObjectsConstructor objectsConstructor,
        IConstructionRecipeCreator constructionRecipeCreator, ILogger logger,
        IProductionLineMover productionLineMover) {
        this.objectsConstructor = objectsConstructor;
        this.constructionRecipeCreator = constructionRecipeCreator;
        this.logger = logger;
        this.productionLineMover = productionLineMover;
    }

    @Override
    protected ConstructionRecipe getConstructionRecipe() {
        return constructionRecipeCreator.getConstructionRecipe();
    }

    @Override
    protected boolean constructObject(ConstructionRecipe constructionRecipe) {
        return objectsConstructor.constructObjectFromRecipe(constructionRecipe);
    }

    @Override
    protected int getNumberOfElementsToProduce() {
        return constructionRecipeCreator.getNumberOfElementsToProduce();
    }

    @Override
    protected void log(LoggingType loggingType, String msg) {
        logger.log(loggingType, msg);
    }

    @Override
    protected boolean moveProductionLine(MovingDirection movingDirection) {
        return this.productionLineMover.moveProductionLine(movingDirection);
    }
}
