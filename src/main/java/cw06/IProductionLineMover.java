package cw06;

public interface IProductionLineMover {
    boolean moveProductionLine(MovingDirection movingDirection);
}
