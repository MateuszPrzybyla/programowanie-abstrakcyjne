package cw06;

public interface IConstructionRecipeCreator {
    int getNumberOfElementsToProduce();
    ConstructionRecipe getConstructionRecipe();
}
