package cw06;

public interface ILogger {
    void log(LoggingType loggingType, String msg);
}
