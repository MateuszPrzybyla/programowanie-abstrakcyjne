package cw02;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class BfsIterator<T> implements Iterator<T> {
    private Queue<Tree<T>> queue = new LinkedList<>();

    public BfsIterator(Tree<T> tree) {
        this.queue.add(tree);
    }

    @Override
    public boolean hasNext() {
        return !this.queue.isEmpty();
    }

    @Override
    public T next() {
        Tree<T> tree = queue.poll();
        queue.addAll(tree.getChildren());
        return tree.getValue();
    }
}
