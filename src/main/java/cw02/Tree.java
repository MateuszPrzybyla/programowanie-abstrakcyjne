package cw02;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Tree<T> implements Iterable<T> {
    private List<Tree<T>> children;
    private T value;
    private EnumeratorOrder order;

    public Tree(T value, EnumeratorOrder order) {
        this(value, order, new LinkedList<>());
    }

    @SuppressWarnings("unchecked")
    public Tree(T value, EnumeratorOrder order, List<Object> children) {
        this.value = value;
        this.order = order;
        this.children = new LinkedList<>();
        for (Object child : children) {
            if (child instanceof Tree) {
                this.children.add((Tree<T>) child);
            } else {
                this.children.add(new Tree(child, order));
            }
        }

    }

    public T getValue() {
        return value;
    }

    public void setOrder(EnumeratorOrder order) {
        this.order = order;
        for (Tree<T> child : this.children) {
            child.setOrder(order);
        }
    }

    public EnumeratorOrder getOrder() {
        return order;
    }

    public List<Tree<T>> getChildren() {
        return children;
    }

    public void add(Tree<T> subtree) {
        this.children.add(subtree);
        subtree.setOrder(this.order);
    }

    public void add(T element) {
        this.children.add(new Tree<T>(element, this.order));
    }

    @Override
    public Iterator<T> iterator() {
        if (order == EnumeratorOrder.DFS) {
            return new DfsIterator<>(this);
        } else {
            return new BfsIterator<>(this);
        }
    }
}
