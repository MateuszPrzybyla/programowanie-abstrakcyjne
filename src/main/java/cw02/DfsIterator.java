package cw02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class DfsIterator<T> implements Iterator<T> {
    private Stack<Tree<T>> stack = new Stack<>();

    public DfsIterator(Tree<T> tree) {
        this.stack.push(tree);
    }

    @Override
    public boolean hasNext() {
        return !this.stack.isEmpty();
    }

    @Override
    public T next() {
        Tree<T> tree = stack.pop();
        List<Tree<T>> children = new ArrayList<>(tree.getChildren());
        Collections.reverse(children);
        children.forEach(child -> stack.push(child));
        return tree.getValue();
    }
}
