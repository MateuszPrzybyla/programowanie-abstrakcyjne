package cw01;

public class TextExporter extends Exporter {
    public TextExporter(String exportedText) {
        this.exportData = new TextData(exportedText);
    }

    @Override
    public TextData getExportData() {
        TextData exportData = (TextData) this.exportData;
        this.exportData = new TextData("");
        return exportData;
    }
}
