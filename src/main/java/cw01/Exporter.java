package cw01;

public abstract class Exporter {
    protected Data exportData;

    public abstract Data getExportData();
}
