package cw01;

public class DistributedModuleFactory {
    private String textToFactory;

    public DistributedModuleFactory(String textToFactory) {
        this.textToFactory = textToFactory;
    }

    public Data createData() {
        return new TextData(textToFactory);
    }

    public Exporter createExporter() {
        return new TextExporter(textToFactory);
    }

    public Importer createImporter() {
        return new TextImporter();
    }
}
