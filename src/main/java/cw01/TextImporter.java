package cw01;

public class TextImporter extends Importer {
    public String getImportedText() {
        return ((TextData) this.importedData).getText();
    }

    @Override
    public void importData(Data dataToImport) {
        if (dataToImport instanceof TextData) {
            super.importData(dataToImport);
        } else {
            throw new IllegalArgumentException("Data to import must be text data");
        }
    }
}
