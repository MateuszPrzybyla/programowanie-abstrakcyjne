package cw01;

public abstract class Importer {
    protected Data importedData;

    public void importData(Data dataToImport) {
        this.importedData = dataToImport;
    }
}
