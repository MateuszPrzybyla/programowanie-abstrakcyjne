package cw03;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

public class TripleList<T> implements Iterable<T> {

    private TripleList<T> previousElement;
    private TripleList<T> middleElement;
    private TripleList<T> nextElement;
    private T value;
    private AtomicInteger size;

    public TripleList() {
        this.size = new AtomicInteger(0);
    }

    private TripleList(T value, AtomicInteger size) {
        this.value = value;
        this.size = size;
    }

    public int size() {
        return size.get();
    }

    public void add(T element) {
        if (this.value == null) {
            this.value = element;
            this.size.incrementAndGet();
        } else if (this.middleElement == null) {
            this.middleElement = new TripleList<>(element, this.size);
            this.middleElement.middleElement = this;
            this.size.incrementAndGet();
        } else if (this.nextElement == null) {
            this.nextElement = new TripleList<>(element, this.size);
            this.nextElement.previousElement = this;
            this.size.incrementAndGet();
        } else {
            this.nextElement.add(element);
        }
    }

    public TripleList<T> getPreviousElement() {
        return previousElement;
    }

    public TripleList<T> getMiddleElement() {
        return middleElement;
    }

    public TripleList<T> getNextElement() {
        return nextElement;
    }

    public T getValue() {
        return value;
    }

    @Override
    public Iterator<T> iterator() {
        return new TripleListIterator<T>(this);
    }
}
