package cw03;

import java.util.Iterator;

public class TripleListIterator<T> implements Iterator<T> {
    private TripleList<T> previous;
    private TripleList<T> current;

    public TripleListIterator(TripleList<T> tripleList) {
        this.current = tripleList;
    }

    @Override
    public boolean hasNext() {
        return current != null && current.getValue() != null;
    }

    @Override
    public T next() {
        T next = current.getValue();
        if (current.getMiddleElement() != previous) {
            previous = current;
            current = current.getMiddleElement();
        } else if (previous != null) {
            TripleList<T> tmpCurrent = current;
            current = previous.getNextElement();
            previous = tmpCurrent;
        } else {
            previous = current;
            current = null;
        }
        return next;
    }
}
